"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const solicitudeController_1 = __importDefault(require("../controllers/solicitudeController"));
class SolicitudeRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/solicitude', solicitudeController_1.default.allSolicitude);
        this.router.get('/solicitude/:id', solicitudeController_1.default.getSolicitude);
        this.router.post('/solicitude/', solicitudeController_1.default.createSolicitude);
        this.router.delete('/solicitude/:id', solicitudeController_1.default.deleteSolicitude);
        this.router.put('/solicitude/:id', solicitudeController_1.default.updateSolicitude);
    }
}
const solicitudeRoutes = new SolicitudeRoutes();
exports.default = solicitudeRoutes.router;

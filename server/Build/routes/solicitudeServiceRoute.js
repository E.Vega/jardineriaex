"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const solicitudeServiceController_1 = __importDefault(require("../controllers/solicitudeServiceController"));
class SolicitudeServiceRouters {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/solicitude/service', solicitudeServiceController_1.default.allSolicitudeService);
        this.router.post('/solicitude/service', solicitudeServiceController_1.default.createSolicitudeService);
        this.router.get('/solicitude/service/:id', solicitudeServiceController_1.default.getSolicitudeService);
        this.router.put('/solicitude/service/:id', solicitudeServiceController_1.default.updateSolicitudeService);
        this.router.delete('/solicitude/service/:id', solicitudeServiceController_1.default.deleteSolicitudeService);
    }
}
const solicitudeServiceRouters = new SolicitudeServiceRouters();
exports.default = solicitudeServiceRouters.router;

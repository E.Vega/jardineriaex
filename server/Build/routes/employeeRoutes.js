"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
class EmployeeRouters {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/employee');
    }
}
const employeeRouters = new EmployeeRouters();
exports.default = employeeRouters.router;

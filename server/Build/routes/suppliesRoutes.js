"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const suppliesController_1 = __importDefault(require("../controllers/suppliesController"));
class SuppliesRouters {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/supplies', suppliesController_1.default.allSupplies);
        this.router.get('/supplies/:id', suppliesController_1.default.getSupplie);
        this.router.post('/supplies/', suppliesController_1.default.createSupplie);
        this.router.delete('/supplies/:id', suppliesController_1.default.deleteSupplie);
        this.router.put('/supplies/:id', suppliesController_1.default.updateSupplie);
    }
}
const supliesRoutes = new SuppliesRouters();
exports.default = supliesRoutes.router;

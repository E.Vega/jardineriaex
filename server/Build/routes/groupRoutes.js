"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const groupController_1 = __importDefault(require("../controllers/groupController"));
class GroupRouters {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/group', groupController_1.default.allGroups);
        this.router.post('/group', groupController_1.default.createGroup);
        this.router.get('/group/:id', groupController_1.default.getGroup);
        this.router.put('/group/:id', groupController_1.default.updateGroup);
        this.router.delete('/group/:id', groupController_1.default.deleteGroup);
        this.router.get('/group/user/:id', groupController_1.default.UsersGroups);
    }
}
const groupRouters = new GroupRouters();
exports.default = groupRouters.router;

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const oracledb_1 = __importDefault(require("oracledb"));
oracledb_1.default.outFormat = oracledb_1.default.OBJECT;
class DataBase {
    close() {
        return __awaiter(this, void 0, void 0, function* () {
            yield oracledb_1.default.getPool().close();
        });
    }
    simpleExecute(statement, binds = [], opts = {}) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let conn;
            try {
                conn = yield oracledb_1.default.getConnection({
                    user: 'jar',
                    password: 'jar',
                    connectString: 'localhost:1521'
                });
                console.log("successfully connected to Oracle!");
                const result = yield conn.execute(statement, binds, opts);
                resolve(result.rows);
            }
            catch (err) {
                reject(err);
            }
            finally {
                if (conn) { // conn assignment worked, need to close
                    try {
                        yield conn.close();
                    }
                    catch (err) {
                        console.log(err);
                    }
                }
            }
        }));
    }
}
exports.dataBase = new DataBase();

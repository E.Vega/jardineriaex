"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database/database");
class UserController {
    allUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield database_1.dataBase.simpleExecute('select * from "user" ');
                res.json(result);
            }
            catch (err) {
                console.error(err);
            }
        });
    }
    login(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const name = yield req.body.user_name;
                const pass = yield req.body.password;
                const user = yield database_1.dataBase.simpleExecute('select * from "user" where user_name = :1 and password = :2', [name, pass]);
                if (user != '') {
                    res.json(user);
                }
                else {
                    res.json({ text: '404' });
                }
            }
            catch (error) {
                console.log(error);
            }
        });
    }
    getUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const query = yield database_1.dataBase.simpleExecute('select * from "user" where user_id = :id', req.params);
                console.log(query);
                res.json(query);
            }
            catch (err) {
                console.error(err);
            }
        });
    }
    createUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const user_name = req.param('user_name');
            const password = req.param('password');
            const type_id = req.param('type_id');
            const group_id = req.param('group_id');
            try {
                yield database_1.dataBase.simpleExecute('insert into "user" (user_id,user_name,password,type_id,group_id) values (user_seq.NEXTVAL,:user_name,:password,:type_id,:group_id)', [user_name, password, type_id, group_id], { autoCommit: true });
                res.json('Usuario Registrado');
            }
            catch (err) {
                console.error(err);
            }
        });
    }
    updateUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const name = req.param('user_name');
            const pass = req.param('password');
            const type = req.param('type_id');
            const group_id = req.param('group_id');
            try {
                yield database_1.dataBase.simpleExecute('update "user" set user_name = :1, password = :2 ,type_id = :3, group_id = :4 where user_id = :id', [name, pass, type, group_id, id], { autoCommit: true });
                res.json('Usuario modificado con exito');
            }
            catch (error) {
                console.log('"Error: ', error);
            }
        });
    }
    deleteUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let conn;
            const { id } = req.params;
            try {
                yield database_1.dataBase.simpleExecute('delete from "user" where user_id = :id', [id], { autoCommit: true });
                res.json('usuario eliminado');
            }
            catch (error) {
                console.log('"Error: ', error);
            }
        });
    }
}
const userController = new UserController();
exports.default = userController;

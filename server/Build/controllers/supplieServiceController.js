"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database/database");
class SuppliesServiceController {
    allSupplieService(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let conn;
            try {
                let result = yield database_1.dataBase.simpleExecute('select * from supplies_service');
                res.json(result);
            }
            catch (error) {
                console.log('"Error: ', error);
            }
        });
    }
    getSupplieService(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                console.log("successfully connected to Oracle!");
                let result = yield database_1.dataBase.simpleExecute('select * from supplie_service where id = :id', [id]);
                res.json(result);
            }
            catch (error) {
                console.log('"Error: ', error);
            }
        });
    }
    createSupplieService(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const count = req.param('count');
            const supplie_id = req.param('supplie_id');
            const service_id = req.param('service_service_id');
            try {
                yield database_1.dataBase.simpleExecute('insert into supplie_service values (supplie_service_seq.NEXTVAL,:1,:2,:3)', [count, supplie_id, service_id], { autoCommit: true });
                res.json('Supplie/service guardado con exito');
            }
            catch (error) {
                console.log('"Error: ', error);
            }
        });
    }
    updateSupplieService(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let conn;
            const { id } = req.params;
            const count = req.param('count');
            const supplie_id = req.param('supplie_id');
            const service_id = req.param('service_service_id');
            try {
                console.log("successfully connected to Oracle!");
                yield database_1.dataBase.simpleExecute('update supplie_service set count = :1, supplie_id = :2 ,service_service_id = :3,  where id = :id', [count, supplie_id, service_id, id], { autoCommit: true });
                res.json('Modificado con exito');
            }
            catch (error) {
                console.log('"Error: ', error);
            }
        });
    }
    deleteSupplieService(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            let conn;
            const { id } = req.params;
            try {
                console.log("successfully connected to Oracle!");
                yield database_1.dataBase.simpleExecute('delete from supplies_service where id = :id', [id], { autoCommit: true });
                res.json({ text: 'usuario eliminado' });
            }
            catch (error) {
                console.log('"Error: ', error);
            }
        });
    }
}
const suppliesServiceController = new SuppliesServiceController();
exports.default = suppliesServiceController;

import oracledb  from 'oracledb';
oracledb.outFormat = oracledb.OBJECT;
class DataBase{

  public async close(){
    await oracledb.getPool().close();
}


public simpleExecute(statement: any, binds : any = [], opts = {}) {
  return new Promise(async (resolve, reject) => {
    let conn;
  
    try {
      conn = await oracledb.getConnection({
        user : 'jar',
        password: 'jar',
        connectString : 'localhost:1521'
      });
      console.log("successfully connected to Oracle!");
      const result  = await conn.execute(statement, binds, opts);
      resolve(result.rows);
    } catch (err) {
      reject(err);
    } finally {
      if (conn) { // conn assignment worked, need to close
        try {
          await conn.close();
        } catch (err) {
          console.log(err);
        }
      }
    }
  });
}


}

export const dataBase = new DataBase();



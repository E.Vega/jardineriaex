import {Request, Response} from 'express';
import {dataBase} from '../database/database'

class SolicitudeController{
    
    public async allSolicitude(req: Request, res: Response){
        try {
            console.log("successfully connected to Oracle!");
            let result = await dataBase.simpleExecute('select * from solicitude');
            res.json(result);
        } catch (error) {
           console.log('"Error: ',error);
        }finally{
            if (dataBase){
                try {
                    await dataBase.close();
                    console.log('connection closed');
                } catch (error) {
                    console.log('Error when closing the database connection: ',error);
                }
            }
        }
    }
    public async getSolicitude(req: Request, res: Response){
        let conn;
        const { id }  = req.params;
        try {
            console.log("successfully connected to Oracle!");
            let result = await dataBase.simpleExecute('select * from solicitude where solicitude_id = :1',[id]);
            res.json(result);
        } catch (error) {
           console.log('"Error: ',error);
        }finally{
            if (dataBase){
                try {
                    await dataBase.close();
                    console.log('connection closed');
                } catch (error) {
                    console.log('Error when closing the database connection: ',error);
                }
            }
        }
    }
    public async createSolicitude(req: Request, res: Response){
        const status = req.param('solicitude_status');
        const user_id = req.param('user_user_id');
        try {
            console.log('successfully connected to Oracle!');
            await dataBase.simpleExecute('insert into solicitude values (solicitude_seq.NEXTVAL,:1,:2,0)',[status,user_id],{autoCommit: true});
            res.json('solicitud creada')
        } catch (error) {
            console.log(error);
            res.json(400)
        }finally{
            if(dataBase){
                try {
                    await dataBase.close();
                    console.log('Connection Closed');
                } catch (error) {
                    console.log('Error when closing the database connection: ',error);
                }
            }
        }
    }
    public async deleteSolicitude(req:Request, res:Response){
        const { id } = req.params;
        try {
            await dataBase.simpleExecute('delete from solicitude where solicitude_id = :id',[id],{autoCommit: true});
            res.json({text: 'solicitud eliminada'})
        } catch (error) {
           console.log('"Error: ',error);
        }finally{
            if (dataBase){
                try {
                    await dataBase.close();
                    console.log('connection closed');
                } catch (error) {
                    console.log('Error when closing the database connection: ',error);
                }
            }
        }
    }
    public async updateSolicitude(req:Request, res: Response){
        let conn;
        const { id } = req.params;
        const status = req.param('solicitude_status');
        const user_id = req.param('user_user_id');
        const grupo_id = req.body.SOLICITUDE_GROUP_ID
        try{
            await dataBase.simpleExecute('update solicitude set solicitude_status = :1, user_user_id = :2,solicitude_group_id = :3 where solicitude_id = :id',[status,user_id,grupo_id],{autoCommit: true});
            res.json('Solicitud modificado con exito');
        }catch (error) {
            console.log('Error: ',error);
        }finally{
            if (conn){
                try {
                    await dataBase.close();
                    console.log('connection closed');
                } catch (error) {
                    console.log('Error when closing the database connection: ',error);
                }
            }

        }
    }

}

const solicitudeController = new SolicitudeController();
export default solicitudeController;
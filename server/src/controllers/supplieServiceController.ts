import {Request,Response} from 'express';
import {dataBase} from '../database/database';

class SuppliesServiceController{

    public async allSupplieService(req: Request, res: Response){
        let conn; 
        try {
            let result = await dataBase.simpleExecute('select * from supplies_service');
            res.json(result);
        } catch (error) {
           console.log('"Error: ',error);
        }
    }
    public async getSupplieService(req: Request, res: Response){
        const { id } = req.params;
        try {
            console.log("successfully connected to Oracle!");
            let result = await dataBase.simpleExecute('select * from supplie_service where id = :id',[id]);
            res.json(result);
        } catch (error) {
           console.log('"Error: ',error);
        }
    }
    public async createSupplieService(req:Request,res:Response){
        const count = req.param('count');
        const supplie_id = req.param('supplie_id');
        const service_id = req.param('service_service_id');
        try{
            await dataBase.simpleExecute('insert into supplie_service values (supplie_service_seq.NEXTVAL,:1,:2,:3)',[count,supplie_id,service_id],{autoCommit: true});
            res.json('Supplie/service guardado con exito');
        }catch (error) {
            console.log('"Error: ',error);
        }
    }
    public async updateSupplieService(req: Request, res: Response){
        let conn;
        const { id } = req.params;
        const count = req.param('count');
        const supplie_id = req.param('supplie_id');
        const service_id = req.param('service_service_id');
        try{
            console.log("successfully connected to Oracle!");
            await dataBase.simpleExecute('update supplie_service set count = :1, supplie_id = :2 ,service_service_id = :3,  where id = :id',
            [count,supplie_id,service_id,id],{autoCommit: true});
            res.json('Modificado con exito');
        }catch (error) {
            console.log('"Error: ',error);
        }
    }
    public async deleteSupplieService(req :Request, res: Response){
        let conn;
        const { id } = req.params;
        try {
            console.log("successfully connected to Oracle!");
            await dataBase.simpleExecute('delete from supplies_service where id = :id',[id],{autoCommit: true});
            res.json({text: 'usuario eliminado'})
        } catch (error) {
           console.log('"Error: ',error);
        }
    }
}
const suppliesServiceController = new SuppliesServiceController();
export default suppliesServiceController;
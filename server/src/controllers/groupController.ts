import {Request, Response} from 'express';
import {dataBase} from '../database/database'

class GroupController{

    public async allGroups(req: Request, res: Response){
        try { 
            let result : any = await dataBase.simpleExecute('select * from "group"');
            res.json(result);
        } catch (error) {
           console.log('"Error: ',error);
        }
    }
    public async getGroup(req: Request, res: Response){
        const { id }  = req.params;
        try {
            let result = await dataBase.simpleExecute('select * from "group" where id = :1',[id]);
            res.json(result);
        } catch (error) {
           console.log('"Error: ',error);
        }
    }
    public async createGroup(req: Request, res: Response){
        const name = req.param('group_name');
        const status = req.param('group_status_id');
        const description = req.body.group_description
        console.log(name)
        try {
            await dataBase.simpleExecute('insert into "group" values (group_seq.NEXTVAL,:group_name,:status,:description)',[name,status,description],{autoCommit: true});
            res.json('Grupo creado')
        } catch (error) {
            console.log(error);
        }
    }
    public async deleteGroup(req:Request, res:Response){
        let conn;
        const { id } = req.params;
        try {
            await dataBase.simpleExecute('delete from "group" where id = :id',[id],{autoCommit: true});
            res.json('grupo eliminado')
        } catch (error) {
           console.log('"Error: ',error);
        }
    }
    public async updateGroup(req:Request, res: Response){
        const { id } = req.params;
        const name = req.param('group_name');
        const status = req.param('group_status_id');
        try{
            await dataBase.simpleExecute('update "group" set group_name = :1, gruop_status_id = :2 where id = :id',[name,status,id],{autoCommit: true});
            res.json('Grupo modificado con exito');
        }catch (error) {
            console.log('Error: ',error);
        }
    }
    public async UsersGroups(req: Request, res: Response){
        const { id }  = req.params;
        try { 
            let result : any = await dataBase.simpleExecute('select * from "user" where group_id = :1',[id]);
            res.json(result);
        } catch (error) {
           console.log('"Error: ',error);
        }
    }

}

const groupController = new GroupController();
export default groupController;
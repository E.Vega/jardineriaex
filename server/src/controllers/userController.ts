import { Request, Response } from 'express';
import { dataBase } from '../database/database';
import jsonwebtoken from 'jsonwebtoken';
import bcrypt from 'bcrypt';
class UserController {

    public async allUser(req: Request, res: Response) {
        try {
            const result = await dataBase.simpleExecute('select * from "user" ');
            res.json(result)
        } catch (err) {
            console.error(err);
        }
    }
    public async login(req: Request, res: Response) {

        try {
            const name = await req.body.user_name
            const pass = await req.body.password
            const user = await dataBase.simpleExecute('select * from "user" where user_name = :1 and password = :2', [name, pass])
            if (user != '') {
                res.json(user)
            } else {
                res.json({ text: '404' })
            }
        } catch (error) {
            console.log(error)
        }
    }
    public async getUser(req: Request, res: Response) {
        try {
            const query = await dataBase.simpleExecute('select * from "user" where user_id = :id', req.params);
            console.log(query);
            res.json(query);
        } catch (err) {
            console.error(err);
        }

    }
    public async createUser(req: Request, res: Response) {

        const user_name = req.param('user_name');
        const password = req.param('password');
        const type_id = req.param('type_id');
        const group_id = req.param('group_id');

        try {
            await dataBase.simpleExecute('insert into "user" (user_id,user_name,password,type_id,group_id) values (user_seq.NEXTVAL,:user_name,:password,:type_id,:group_id)', [user_name, password, type_id, group_id], { autoCommit: true });
            res.json('Usuario Registrado');
        } catch (err) {
            console.error(err);
        }


    }
    public async updateUser(req: Request, res: Response) {
        const { id } = req.params;
        const name = req.param('user_name');
        const pass = req.param('password');
        const type = req.param('type_id');
        const group_id = req.param('group_id');
        try {
            await dataBase.simpleExecute('update "user" set user_name = :1, password = :2 ,type_id = :3, group_id = :4 where user_id = :id', [name, pass, type, group_id, id], { autoCommit: true });
            res.json('Usuario modificado con exito');
        } catch (error) {
            console.log('"Error: ', error);
        }
    }
    public async deleteUser(req: Request, res: Response) {
        let conn;
        const { id } = req.params;
        try {
            await dataBase.simpleExecute('delete from "user" where user_id = :id', [id], { autoCommit: true });
            res.json('usuario eliminado')
        } catch (error) {
            console.log('"Error: ', error);
        }
    }
}
const userController = new UserController();
export default userController;
import {Request, Response} from 'express';
import {dataBase} from '../database/database'

class SolicitudeServiceController{
    
    public async allSolicitudeService(req: Request, res: Response){
        let conn; 
        try {
            console.log("successfully connected to Oracle!");
            let result = await dataBase.simpleExecute('select * from solicitude_service');
            res.json(result);
        } catch (error) {
           console.log('"Error: ',error);
        }
    }
    public async getSolicitudeService(req: Request, res: Response){
        const { id }  = req.params;
        try {
            console.log("successfully connected to Oracle!");
            let result = await dataBase.simpleExecute('select * from solicitude_service where solicitude_solicitude_id = :1',[id]);
            res.json(result);
        } catch (error) {
           console.log('"Error: ',error);
        }
    }
    public async createSolicitudeService(req: Request, res: Response){
        const address = req.param('address');
        const status = req.param('status');
        const date = new Date();
        const solicitude_id = req.param('solicitude_solicitude_id');
        const service_id = req.param('service_service_id');
        try {
            await dataBase.simpleExecute('insert into solicitude_service values (:1,:2,:3,:4,:5)',[address,status,date,solicitude_id,service_id],{autoCommit: true});
            res.json('solicitud/servicio creado')
        } catch (error) {
            console.log(error);
        }
    }
    public async deleteSolicitudeService(req:Request, res:Response){
        let conn;
        const { id } = req.params;
        try {
            await dataBase.simpleExecute('delete from solicitude_service where solicitude_solicitude_id = :id',[id],{autoCommit: true});
            res.json({text: 'solicitud eliminada'})
        } catch (error) {
           console.log('"Error: ',error);
        }
    }
    public async updateSolicitudeService(req:Request, res: Response){
        let conn;
        const { id } = req.params;
        const address = req.param('address');
        const status = req.param('status');
        const solicitude_id = req.param('solicitude_solicitude_id');
        const service_id = req.param('service_service_id');
        try{
            await dataBase.simpleExecute('update solicitude_service set address = :1, status = :2, solicitude_solicitude_id = :4, service_service_id =:5 where solicitude_solicitude_id = :id',
            [address,status,solicitude_id,service_id],{autoCommit: true});
            res.json('Solicitud/servicio modificado con exito');
        }catch (error) {
            console.log('Error: ',error);
        }
    }

}

const solicitudeServiceController = new SolicitudeServiceController();
export default solicitudeServiceController;
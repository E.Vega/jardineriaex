import {Request, Response} from 'express';
import {dataBase} from '../database/database'

class ServiceController{

    public async allServices(req: Request, res: Response){
        try {
            let result = await dataBase.simpleExecute('select * from service ');
            res.json(result);
        } catch (error) {
           console.log('"Error: ',error);
        }
    }
    public async getService(req: Request, res: Response){
        const { id }  = req.params;
        try {
            let result = await dataBase.simpleExecute('select * from service where service_id = :1',[id]);
            res.json(result);
        } catch (error) {
           console.log('"Error: ',error);
        }
    }
    public async createService(req: Request, res: Response){
        let conn;
        const name = req.param('service_description');
        const description = req.param('service_name');
        try {
            await dataBase.simpleExecute('insert into service values (service_seq.NEXTVAL,:1,:2)',[description,name],{autoCommit: true});
            res.json('Servicio creado')
        } catch (error) {
            console.log(error);
        }
    }
    public async deleteService(req:Request, res:Response){
        let conn;
        const { id } = req.params;
        try {
            await dataBase.simpleExecute('delete from service where service_id = :id',[id],{autoCommit: true});
            res.json({text: 'servicio eliminado'})
        } catch (error) {
           console.log('"Error: ',error);
        }
    }
    public async updateService(req:Request, res: Response){
        const { id } = req.params;
        const name = req.param('service_name');
        const description = req.param('service_description');
        try{
            await dataBase.simpleExecute('update service set service_name = :1, service_description = :2 where service_id = :id',[name,description,id],{autoCommit: true});
            res.json('Servicio modificado con exito');
        }catch (error) {
            console.log('Error: ',error);
        }
    }

}

const serviceController = new ServiceController();
export default serviceController;
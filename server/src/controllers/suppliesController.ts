import {Request, Response} from 'express';
import {dataBase} from '../database/database'

class SuppliesController{

    public async allSupplies(req: Request, res: Response){
        try {
            let result = await dataBase.simpleExecute('select * from supplies');
            res.json(result);
        } catch (error) {
           console.log('"Error: ',error);
        }
    }
    public async getSupplie(req: Request, res: Response){
        let conn;
        const { id }  = req.params;
        try {
            let result = await dataBase.simpleExecute('select * from supplies where id = :1',[id]);
            res.json(result);
        } catch (error) {
           console.log('"Error: ',error);
        }
    }
    public async createSupplie(req: Request, res: Response){
        let conn;
        const supplies_description = req.param('supplies_description');
        const supplies_name = req.param('supplies_name');
        try {
            await dataBase.simpleExecute('insert into supplies values (:supplies_description,:supplies_name,supplies_seq.NEXTVAL)',[supplies_description,supplies_name],{autoCommit: true});
            res.json('supplie creado')
        } catch (error) {
            console.log(error);
        }
    }
    public async deleteSupplie(req:Request, res:Response){
        let conn;
        const { id } = req.params;
        try {
            await dataBase.simpleExecute('delete from supplies where id = :id',[id],{autoCommit: true});
            res.json({text: 'sup eliminado'})
        } catch (error) {
           console.log('"Error: ',error);
        }
    }
    public async updateSupplie(req:Request, res: Response){
        let conn;
        const { id } = req.params;
        const description = req.param('supplies_description');
        const name = req.param('supplies_name');
        try{
            await dataBase.simpleExecute('update supplies set supplies_description = :1, supplies_name = :2 where id = :id',[description,name,id],{autoCommit: true});
            res.json('Supplie modificado con exito');
        }catch (error) {
            console.log('Error: ',error);
        }
    }

}

const suppliesController = new SuppliesController();
export default suppliesController;
import {Router} from 'express';
import userController from '../controllers/userController';
class UserRouters{
    router: Router = Router();
    constructor(){
        this.config();
    }
    config(): void{
        this.router.get('/user',userController.allUser);
        this.router.post('/user',userController.createUser);
        this.router.get('/user/:id',userController.getUser);
        this.router.put('/user/:id',userController.updateUser);
        this.router.delete('/user/:id',userController.deleteUser);
        this.router.post('/user/login/',userController.login)
    }
}
const userRouters = new UserRouters();
export default userRouters.router;
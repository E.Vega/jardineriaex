import {Router} from 'express';
import solicitudeController from '../controllers/solicitudeController';

class SolicitudeRoutes{
    router: Router = Router();

    constructor(){
        this.config();
    }
    config(){
        this.router.get('/solicitude',solicitudeController.allSolicitude);
        this.router.get('/solicitude/:id',solicitudeController.getSolicitude);
        this.router.post('/solicitude/',solicitudeController.createSolicitude);
        this.router.delete('/solicitude/:id',solicitudeController.deleteSolicitude);
        this.router.put('/solicitude/:id',solicitudeController.updateSolicitude);
    }
}

const solicitudeRoutes = new SolicitudeRoutes();
export default solicitudeRoutes.router;
import {Router} from 'express';
import suppliesController from '../controllers/suppliesController';

class SuppliesRouters{
    router: Router = Router();

    constructor(){
        this.config();
    }
    config(){
        this.router.get('/supplies',suppliesController.allSupplies);
        this.router.get('/supplies/:id',suppliesController.getSupplie);
        this.router.post('/supplies/',suppliesController.createSupplie);
        this.router.delete('/supplies/:id',suppliesController.deleteSupplie);
        this.router.put('/supplies/:id',suppliesController.updateSupplie);
    }

}

const supliesRoutes = new SuppliesRouters();
export default supliesRoutes.router;
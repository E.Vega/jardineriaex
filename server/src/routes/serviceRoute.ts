import {Router} from 'express';
import serviceController from '../controllers/serviceController';
class ServiceRouters{
    router: Router = Router();
    constructor(){
        this.config();
    }
    config(): void{
        this.router.get('/service',serviceController.allServices);
        this.router.post('/service',serviceController.createService);
        this.router.get('/service/:id',serviceController.getService);
        this.router.put('/service/:id',serviceController.updateService);
        this.router.delete('/service/:id',serviceController.deleteService);
    }
}
const serviceRouters = new ServiceRouters();
export default serviceRouters.router;
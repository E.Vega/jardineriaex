import {Router} from 'express';
import solicitudeServiceController from '../controllers/solicitudeServiceController';
class SolicitudeServiceRouters{
    router: Router = Router();
    constructor(){
        this.config();
    }
    config(): void{
        this.router.get('/solicitude/service',solicitudeServiceController.allSolicitudeService);
        this.router.post('/solicitude/service',solicitudeServiceController.createSolicitudeService);
        this.router.get('/solicitude/service/:id',solicitudeServiceController.getSolicitudeService);
        this.router.put('/solicitude/service/:id',solicitudeServiceController.updateSolicitudeService);
        this.router.delete('/solicitude/service/:id',solicitudeServiceController.deleteSolicitudeService);
    }
}
const solicitudeServiceRouters = new SolicitudeServiceRouters();
export default solicitudeServiceRouters.router;